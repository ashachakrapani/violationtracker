//
//  ViolationSummaryTableViewCell.swift
//  ViolationsTracker
//
//  Created by Asha Chakrapani on 8/22/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import UIKit

class ViolationSummaryTableViewCell: UITableViewCell {
   
    @IBOutlet weak var violationCategory: UILabel!
    @IBOutlet weak var totalNumberOfViolationsLabel: UILabel!
    @IBOutlet weak var totalNumberOfViolations: UILabel!
    @IBOutlet weak var earliestDateLabel: UILabel!
    @IBOutlet weak var earliestDate: UILabel!
    @IBOutlet weak var latestDateLabel: UILabel!
    @IBOutlet weak var latestDate: UILabel!
    
    func resetCell() {
        self.totalNumberOfViolations.text = ""
        self.violationCategory.text = ""
        self.earliestDate.text = ""
        self.latestDate.text = ""
    }
    
}
