//
//  ViolationSummary.swift
//  ViolationsTracker
//
//  Created by Asha Chakrapani on 8/22/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import Foundation

//MARK: - Model representing the required details to be displayed for the violation summary
public struct ViolationSummary {
    var category: ViolationCategory?
    var totalNumberOfViolations: Int?
    var earliestDate: Date?
    var latestDate: Date?
}
