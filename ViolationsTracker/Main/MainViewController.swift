//
//  ViewController.swift
//  ViolationsTracker
//
//  Created by Asha Chakrapani on 8/21/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import UIKit
import Foundation


class MainViewController: UIViewController {
    
    //MARK: - UIViewcontroller overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 75
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.setUpNavigationBar()
        
        self.processViolationData()
    }
    
    //MARK: - IBOutlet
    @IBOutlet var tableView: UITableView!
    

    //MARK: - Private API
    private lazy var violationSummaryList: [ViolationSummary] = []
    private var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    private func setUpNavigationBar() {
        self.navigationItem.title = NSLocalizedString("Violation Summary", comment: "Title for violation summary table")
    }
    
    private func getCSVFileContents() -> String? {
        
        if let filepath = Bundle.main.path(forResource: "C4C-dev-challenge-2018", ofType: "csv") {
            do {
                return try String(contentsOfFile: filepath, encoding: .utf8)
            } catch {
                print("error while attempting to read the contents of csv file")
            }
        }
        return nil
    }
    
    private func read(content: String, separator: String) -> [String] {
        return content.components(separatedBy: separator)
    }
    
    private func createNewViolationSummary(forRowContent rowContent: [String]) -> ViolationSummary {
        var createdDate: Date? = nil
        
        let createdDateComponents = rowContent[3].components(separatedBy: " ")
        if  let _createdDate = createdDateComponents.first {
            createdDate = self.dateFormatter.date(from: _createdDate)
        }
        
        let newSummaryItem = ViolationSummary(category: ViolationCategory.init(rawValue: rowContent[2]),
                                              totalNumberOfViolations: 1,
                                              earliestDate: createdDate,
                                              latestDate: createdDate)
        return newSummaryItem
    }
    
    private func processViolationData() {
        DispatchQueue.global().async {
            //Get file contents
            let _fileContents = self.getCSVFileContents()
            guard let fileContents = _fileContents else { return }
            //Get rows
            let rows = self.read(content: fileContents, separator: "\n").dropFirst(1)
            //Parse through each row and process data as required
            for row in rows {
                let rowContent = self.read(content: row, separator: ",")
                
                if let indexOfExistingSummaryItem = self.violationSummaryList.firstIndex(where: { $0.category ?? ViolationCategory.Unknown == ViolationCategory.init(rawValue: rowContent[2]) }) {
                    let existingViolation = self.violationSummaryList[indexOfExistingSummaryItem]
                    
                    //Up the count
                    var totalNoOfViolations = existingViolation.totalNumberOfViolations ?? 0
                    totalNoOfViolations += 1
                    
                    var newViolationSummary = self.createNewViolationSummary(forRowContent: rowContent)
                    newViolationSummary.totalNumberOfViolations = totalNoOfViolations
                    
                    //Determine the earliest and latest dates of violation
                    if let _existingEarliestDate = existingViolation.earliestDate, let _newEarliestDate = newViolationSummary.earliestDate {
                        if (_existingEarliestDate < _newEarliestDate ) {
                            newViolationSummary.earliestDate = _existingEarliestDate
                        }
                    }
                    
                    if let _existingLatestDate = existingViolation.latestDate, let _newLatestDate = newViolationSummary.latestDate {
                        if (_existingLatestDate > _newLatestDate ) {
                            newViolationSummary.latestDate = _existingLatestDate
                        }
                    }
                    
                    self.violationSummaryList[indexOfExistingSummaryItem] = newViolationSummary
                } else {
                    self.violationSummaryList.append(self.createNewViolationSummary(forRowContent: rowContent))
                }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.violationSummaryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ViolationSummaryCell", for: indexPath) as! ViolationSummaryTableViewCell
        cell.resetCell()
        
        let violationSummaryItem = self.violationSummaryList[indexPath.row]
        cell.violationCategory.text = violationSummaryItem.category.map { $0.rawValue }
        if let _totalNumberOfViolations = violationSummaryItem.totalNumberOfViolations {
            cell.totalNumberOfViolations.text =  String(_totalNumberOfViolations)
        }
        
        if let _earliestDate = violationSummaryItem.earliestDate {
            cell.earliestDate.text = self.dateFormatter.string(from: _earliestDate)
        }
        
        if let _latestDate = violationSummaryItem.latestDate {
            cell.latestDate.text = self.dateFormatter.string(from: _latestDate)
        }
        
        cell.isUserInteractionEnabled = false
        return cell
    }
    
}
