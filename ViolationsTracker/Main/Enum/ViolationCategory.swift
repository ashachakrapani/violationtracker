//
//  ViolationCategory.swift
//  ViolationsTracker
//
//  Created by Asha Chakrapani on 8/22/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

//MARK: - Enum representing the availble ViolationCategory
public enum ViolationCategory: String {
    case GarbageAndRefuse = "Garbage and Refuse"
    case UnsanitaryConditions = "Unsanitary Conditions"
    case AnimalsAndPests = "Animals and Pests"
    case Vegetation = "Vegetation"
    case BuildingConditions = "Building Conditions"
    case ChemicalHazards = "Chemical Hazards"
    case Biohazards = "Biohazards"
    case AirPollutantsAndOdors = "Air Pollutants and Odors"
    case RetailFood = "Retail Food"
    case Unknown = "Unknown"
}
